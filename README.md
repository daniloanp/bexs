# Desafio Bexs

Essa é a implementação do desafio da bexs.

### Pré-requisitos

Esse projeto foi desenvolvido com o python 3.8.4 e utiliza o pipenv para cuidar das dependências.
Opcionalmente, existe também uma forma de rodar no docker.

Portanto, recomendamos:

- Ter uma versão do python 3.8.4 (ver [pyenv](https://github.com/pyenv/pyenv))
- Ter o pipenv instalado (pip install pipenv costumar funcionar, [veja!](https://pypi.org/project/pipenv/)
- Opcionalmente ter a ferramenta Make instalada
- Opcionalmente ter uma versão do docker >= 19 

### Instalando as dependências

Para executar basta rodar o `pipenv`
- pipenv install

### Rodando no modo CLI
Basta rodar o comando abaixo.
- pipenv run python -m src.cli resources/input-file.txt

Se tiver o make recomendo rodar com `make run-cli`

Se tiver o make e o Docker pode rodar com `make run-cli-docker`


### Rodando no modo web_service
Basta rodar o comando abaixo.
- ROUTES_FILE_PATH=resources/input-file.txt pipenv run python -m src.web_service 
Se tiver o make recomendo rodar com `make run-api`

Se tiver o make e o Docker pode rodar com `make run-api-docker`

### Documentação da API HTTP
No modo web_service existem 3 endpoints:
- GET http://localhost:8080/routes
    * Lista as rotas existentes no formato: 
```json
[
    {
        "cost": 10,
        "source": "GRU",
        "destination": "BRC"
    }
]
```
- POST http://localhost:8080/routes
    * Cadastra uma rota no arquivo, recebendo o seguinte corpo:
```json
{
    "cost": 10,
    "source": "GRU",
    "destination": "BRC"
}
```
- GET http://localhost:8080/itinerary?origin=GRU&destination=SCL
    * Retorna o Itineráio de menor custo
```json
{
    "cost": 15,
    "routes": [
        {
            "cost": 10,
            "origin": "GRU",
            "destination": "BRC"
        },
        {
            "cost": 5,
            "origin": "BRC",
            "destination": "SCL"
        }
    ]
}
``` 

### Estrutura do projeto:
- src - Pasta com códigos de execução
    * cli/
        + . Arquivos implementando interface cli
    * web_service/
        + . rquivos implementando interface web
    * . Arquivos com lógica de cálculo de rota
- test - Pasta com códigos de teste
    * test_cli/
        + . Arquivos de teste da interface cli
    * test_web_service/
        + . Arquivos de teste da interface web
    * Arquivos de teste da lógica de negócio
- Dockerfile
- Pipfile
- Pipfile.lock 
- setup.cfg
- README.md

### Decisões de design

- Decidi separar a lógica do algoritmo de dijkstra da lógica de gerenciamento. 
- Implementei uma matriz de adjacência simples que lida apenas com inteiros, o que torna fácil de otimizar e reusar.
- Separei também A lógica de administração das interfaces (CLI e web) para separar bem a responsabilidade
- Decidi usar o framework aiohttp por ser o que mais uso e ele escalar bem se for necessário aumenta a implementação
- Decidi usar um repositório para facilitar uma eventual migração pra uma outra forma de persistência.
- Usei o framework aiofiles pois o modelo de concorrência asyncio do python que é usado pelo 3.5 + não lida bem com as funções padrão "open"