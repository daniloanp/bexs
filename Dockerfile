FROM python:3.8.4-alpine as base-image
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH=/app/src/
ENV PATH="/root/venv/bin:$PATH"


WORKDIR /app

RUN pip install -U pip setuptools pipenv;
RUN python -m venv /root/venv
RUN apk add --update --no-cache --virtual .build-deps  alpine-sdk curl wget build-base

COPY ./src /app/src
COPY ./test /app/test
COPY ./Pipfile.lock ./Pipfile /app/
COPY ./setup.cfg /app/

RUN  pipenv install --system --deploy

FROM base-image as test-image

RUN  pipenv install --system --deploy -d

CMD pytest --cov

FROM python:3.8.4-alpine as production-image

ENV PYTHONUNBUFFERED 1

COPY --from=base-image /root/venv /root/venv
COPY --from=base-image /app/src /app/src
ENV PATH="/root/venv/bin:$PATH"

WORKDIR /app
COPY resources/input-file.txt /app/
ENV ROUTES_FILE_PATH=/app/input-file.txt

CMD python -m src.web_service

