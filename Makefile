.PHONY: lint run-cli run-api test test-docker run-api-docker make run-cli-docker
export PYTHONPATH=.

lint:
	pipenv sync -d
	flake8
run-cli:
	pipenv sync -d
	pipenv run python -m src.cli resources/input-file.txt
run-api:
	pipenv sync -d
	ROUTES_FILE_PATH=resources/input-file.txt pipenv run python -m src.web_service
test:
	pipenv sync -d
	pipenv run pytest --cov
test-docker:
	docker build --target test-image -t bexs-test .
	docker run bexs-test
run-api-docker:
	docker build -t bexs-api .
	docker run  -p 8080:8080 bexs-api
run-cli-docker:
	docker build -t bexs-api .
	docker run -it bexs-api python -m src.cli /app/input-file.txt
