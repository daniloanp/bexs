import sys

from src.cli.manager import CliManager
from src.routes_manager import routes_manager_from_file_path


def main() -> None:
    assert len(sys.argv) == 2, 'Just one argument: the name of existing routes'
    routes_filepath = sys.argv[1]
    manager = routes_manager_from_file_path(routes_filepath)
    CliManager(manager).input_loop()


if __name__ == '__main__':
    main()
