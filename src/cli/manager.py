import re
from typing import Tuple

from src.routes_manager import Itinerary, RoutesManager, LocationNotFoundException, OriginSameAsDestinationException


class InvalidInput(BaseException):
    def __init__(self, user_input):
        self._input = user_input


def format_itinerary(itinerary: Itinerary) -> str:
    locations = [itinerary.routes[0].origin] + [route.destination for route in itinerary.routes]
    return ' - '.join(locations) + f" > ${itinerary.cost}"


class CliManager:
    _INPUT_REGEXP = re.compile('[0-9a-zA-Z]+-[0-9a-zA-Z]+')

    def __init__(self, manager: RoutesManager):
        self._manager = manager

    def _input(self) -> Tuple[str, str]:
        user_input = input("please enter the route:")
        if self._INPUT_REGEXP.match(user_input):
            origin, destination = user_input.strip().split("-")
            return origin, destination
        raise InvalidInput("invalid input")

    def input_loop(self) -> None:
        while True:
            try:
                origin, destination = self._input()
                best_route = self._manager.get_best_route(origin, destination)
                if best_route is None:
                    print("no route found")
                else:
                    print("best route: ", format_itinerary(best_route))
            except InvalidInput:
                print("invalid input, please enter according to ORG-DST")
            except LocationNotFoundException as e:
                print("invalid location", e.location + ", please enter one of the following:",
                      ', '.join(self._manager.locations()))
            except OriginSameAsDestinationException as e:
                print('origin and destination are the same: ', e.location, 'please try again!')
            except KeyboardInterrupt:
                print("\n\n>>>thank you for using our system, good bye!!!")
                break
