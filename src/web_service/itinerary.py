from http import HTTPStatus
from typing import Dict, Any

from aiohttp import web

from src.routes_manager import LocationNotFoundException, OriginSameAsDestinationException, Itinerary
from src.web_service.routes_repository import RoutesRepository


def itinerary_to_dict(itinerary: Itinerary) -> Dict[str, Any]:
    return {
        'cost': itinerary.cost,
        'routes': [dict(cost=r.cost, origin=r.origin, destination=r.destination) for r in itinerary.routes]
    }


class ItineraryView(web.View):
    @property
    def _routes_repository(self) -> RoutesRepository:
        return self.request.app['RoutesRepository']

    async def get(self) -> web.Response:
        origin = self.request.query.get('origin')
        destination = self.request.query.get('destination')
        routes_manager = await self._routes_repository.retrieve_manager()
        if origin is None or destination is None:
            return web.json_response(data={'message': 'query params "destination" and "origin" must be suplied'},
                                     status=HTTPStatus.BAD_REQUEST)
        try:
            best_route = routes_manager.get_best_route(origin, destination)
            if best_route is None:
                return web.json_response(data={"message": "no route found"}, status=HTTPStatus.NOT_FOUND)
            else:
                return web.json_response(data=itinerary_to_dict(best_route), status=HTTPStatus.OK)
        except LocationNotFoundException as e:
            message = "invalid location", e.location + \
                      ", please enter one of the following:" + \
                      ', '.join(routes_manager.locations())
            return web.json_response(data={"message": message}, status=HTTPStatus.BAD_REQUEST)
        except OriginSameAsDestinationException as e:
            message = 'origin and destination are the same: ' + \
                      e.location + \
                      ' please try again!'
            return web.json_response(data={"message": message}, status=HTTPStatus.BAD_REQUEST)
