import os
import socket
import sys
from datetime import datetime

import aiofiles
from aiohttp import web

from src.web_service.config import Config
from src.web_service.itinerary import ItineraryView
from src.web_service.routes import RoutesView
from src.web_service.routes_repository import RoutesRepository


def create_application(config: Config) -> web.Application:
    app = web.Application()

    async def file_context(_) -> None:
        async with aiofiles.open(config.routes_file_path, 'a+') as f:
            app['file'] = f
            app['RoutesRepository'] = RoutesRepository(f)
            await app['RoutesRepository'].load_manager()
            yield

    app.cleanup_ctx.append(file_context)

    async def base(_: web.Request) -> web.Response:
        return web.json_response(data=dict(
            hostname=socket.gethostname(),
            server_current_timestamp=datetime.now().astimezone().isoformat(),
            os=str(os.uname()),
            sys=sys.version
        ))

    app.add_routes([web.get('/', base),
                    web.view('/itinerary', ItineraryView),
                    web.view('/routes', RoutesView),
                    ])

    return app
