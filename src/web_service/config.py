from pydantic import BaseSettings


class Config(BaseSettings):
    port: int = 8080
    routes_file_path: str
