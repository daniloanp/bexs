import asyncio
from typing import List, Optional

from aiofiles.threadpool import AsyncFileIO

from src.routes_manager import Route, RoutesManager


class RoutesRepository:
    def __init__(self, file: AsyncFileIO):
        self._file = file
        self._data: List[Route]
        self._lock = asyncio.Lock()
        self._routes_manager: Optional[RoutesManager] = None

    async def save(self, origin: str, destination: str, cost: int) -> None:
        async with self._lock:
            await self._file.write(f'\n{origin},{destination},{cost}')
            self._routes_manager = None

    async def load_manager(self) -> None:
        async with self._lock:
            await self._file.seek(0)
            routes = []
            async for line in self._file:
                data = line.split(',')
                assert 3 == len(data), 'must contains 3 elements per file'
                routes.append(Route(data[0], data[1], int(data[2])))
            self._routes_manager = RoutesManager(routes)

    async def retrieve_manager(self) -> RoutesManager:
        if self._routes_manager is None:
            await self.load_manager()
        return self._routes_manager
