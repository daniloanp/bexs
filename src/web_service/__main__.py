from aiohttp import web

from src.web_service.app import create_application
from src.web_service.config import Config

if __name__ == '__main__':
    config = Config()
    app = create_application(config)
    web.run_app(app, port=config.port)
