from http import HTTPStatus

from aiohttp import web
from pydantic import BaseModel, ValidationError

from src.web_service.routes_repository import RoutesRepository


class Body(BaseModel):
    origin: str
    destination: str
    cost: int


class RoutesView(web.View):
    def __init__(self, request: web.Request):
        super().__init__(request)

    @property
    def _routes_repository(self) -> RoutesRepository:
        return self.request.app['RoutesRepository']

    async def get(self) -> web.Response:
        routes_manager = await self._routes_repository.retrieve_manager()

        return web.json_response(data=[
            dict(cost=r.cost, source=r.origin, destination=r.destination)
            for r in routes_manager.routes()
        ])

    async def post(self):
        if self.request.content_type != 'application/json':
            return web.json_response(data={'message': 'please pass a valid json'},
                                     status=HTTPStatus.UNSUPPORTED_MEDIA_TYPE)

        try:
            data = await self.request.json()
            body = Body(**data)
            await self._routes_repository.save(body.origin, body.destination, body.cost)
            return web.json_response(data={'message': 'created'}, status=HTTPStatus.CREATED)
        except ValidationError as e:
            return web.json_response(status=HTTPStatus.BAD_REQUEST, data=e.json())
