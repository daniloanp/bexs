import math
import sys
from dataclasses import dataclass
from functools import lru_cache
from typing import List, Iterable, Set, Optional, Union

from src.matrix2d import Matrix2d


@dataclass
class Edge:
    origin: int
    destination: int
    cost: int


@dataclass
class Path:
    edges: List[Edge]
    cost: int
    destination: int


Number = Union[int, float]


class Graph:
    # adjacency_list: List[List[Edge]]

    def __init__(self, num_vertices: int, edges: Iterable[Edge]):
        self._num_vertices = num_vertices

        self._adjacency_matrix = Matrix2d(num_vertices, num_vertices)

        for edge in edges:
            assert edge.origin < self._num_vertices
            assert edge.destination < self._num_vertices
            self._adjacency_matrix.set(edge.origin, edge.destination, edge.cost)

    @staticmethod
    def _choose_closest(distance_to_vertx: List[Number], short_path_tree: Set[int]) -> Optional[int]:
        min_distance = sys.maxsize
        min_index = None
        for v in short_path_tree:
            if distance_to_vertx[v] < min_distance:
                min_distance = distance_to_vertx[v]
                min_index = v

        return min_index

    @lru_cache
    def dijkstra(self, source: int) -> List[Path]:
        distances_to_vertx = [math.inf] * self._num_vertices
        path_to_vertx = [[] for _ in range(self._num_vertices)]
        distances_to_vertx[source] = 0
        path_to_vertx[source] = [source]
        unvisited = set(range(self._num_vertices))

        for _ in range(self._num_vertices):
            closest = self._choose_closest(distances_to_vertx, unvisited)
            if closest is None:
                continue
            unvisited.remove(closest)

            # Update dist value of the adjacent vertices
            # of the picked vertex only if the current
            # distance is greater than new distance and
            # the vertex in not in the shotest path tree
            for vertx in range(self._num_vertices):
                distance_from_closest = self._adjacency_matrix.get(closest, vertx)
                if distance_from_closest \
                        and vertx in unvisited \
                        and distances_to_vertx[vertx] > distances_to_vertx[closest] + distance_from_closest:
                    distances_to_vertx[vertx] = distances_to_vertx[closest] + distance_from_closest
                    path_to_vertx[vertx] = path_to_vertx[closest] + [vertx]

        paths = []
        for vertx in range(self._num_vertices):
            if distances_to_vertx[vertx] == math.inf:
                continue
            path = path_to_vertx[vertx]
            edges_iterator = zip(path[:-1], path[1:])

            edges = [Edge(origin=edge[0], destination=edge[1], cost=self._adjacency_matrix.get(edge[0], edge[1]))
                     for edge in edges_iterator]

            paths.append(Path(edges=edges, cost=int(distances_to_vertx[vertx]), destination=vertx))

        return paths
