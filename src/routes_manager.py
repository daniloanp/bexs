from csv import DictReader
from dataclasses import dataclass
from typing import Dict, Optional, Tuple, Iterable

from src.graph import Graph, Edge


@dataclass
class Route:
    origin: str
    destination: str
    cost: int


@dataclass
class Itinerary:
    cost: int
    routes: Tuple[Route, ...]


class LocationNotFoundException(BaseException):
    def __init__(self, location: str):
        self.location = location


class OriginSameAsDestinationException(BaseException):
    def __init__(self, location: str):
        self.location = location


class RoutesManager:
    def __init__(self, routes: Iterable[Route]):
        self._locations_names_to_ids: Dict[str, int] = {}
        self._locations_ids_to_names: Dict[int, str] = {}
        self._auto_increment_id = 0
        self._routes = tuple(routes)

        edges = []
        for route in self._routes:
            origin = self._get_location_id(route.origin)
            destination = self._get_location_id(route.destination)
            edges.append(Edge(origin=origin, destination=destination, cost=route.cost))
        self._graph = Graph(len(self._locations_ids_to_names), edges=edges)

    def _get_location_id(self, location: str) -> int:
        id_ = self._locations_names_to_ids.get(location)
        if id_ is not None:
            return id_
        id_ = self._auto_increment_id
        self._auto_increment_id += 1
        self._locations_names_to_ids[location] = id_
        self._locations_ids_to_names[id_] = location
        return id_

    def routes(self) -> Iterable[Route]:
        return self._routes

    def locations(self) -> Iterable[str]:
        return self._locations_names_to_ids.keys()

    def _edges_to_routes(self, edges: Iterable[Edge]) -> Tuple[Route, ...]:
        return tuple(
            Route(origin=self._locations_ids_to_names[edge.origin],
                  destination=self._locations_ids_to_names[edge.destination],
                  cost=edge.cost,
                  )
            for edge in edges
        )

    def get_best_route(self, origin: str, destination: str) -> Optional[Itinerary]:
        """
        Returns every route from origin
        :return:
        """
        if origin == destination:
            raise OriginSameAsDestinationException(origin)
        try:
            origin_id = self._locations_names_to_ids[origin]
            destination_id = self._locations_names_to_ids[destination]
        except KeyError as e:
            raise LocationNotFoundException(e.args[0])
        paths = self._graph.dijkstra(origin_id)

        paths_to_destination = filter(lambda p: p.destination == destination_id, paths)

        for path in paths_to_destination:
            routes = self._edges_to_routes(path.edges)
            return Itinerary(routes=routes, cost=path.cost)

        return None


def routes_manager_from_file_path(file_path: str) -> RoutesManager:
    with open(file_path, 'r') as routes_file:
        reader = DictReader(routes_file, fieldnames=['origin', 'destination', 'cost'])
        routes = (Route(origin=row['origin'], destination=row['destination'], cost=int(row['cost']))
                  for row in reader)

        return RoutesManager(routes)
