from typing import Optional, List


class Matrix2d:
    def __init__(self, number_of_rows: int, number_of_columns: int):
        self._num_rows = number_of_rows
        self._num_columns = number_of_columns
        self._list: List[Optional[int]] = list(None for _ in range(number_of_rows * number_of_columns))

    def get(self, n: int, m: int) -> Optional[int]:
        return self._list[n * self._num_rows + m]

    def set(self, n: int, m: int, value: int) -> None:
        self._list[n * self._num_rows + m] = value
