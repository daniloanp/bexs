from unittest import TestCase

from src.graph import Graph, Edge, Path


class TestGraph(TestCase):
    def test_invalid_edge_must_return_assertion_error(self):
        with self.assertRaises(AssertionError):
            Graph(3, [
                Edge(origin=100, destination=2, cost=5),
            ])

    def test_disconnected_graph_must_returns_only_path_to_it_self(self):
        g = Graph(10, [
        ])

        for i in range(10):
            distances = g.dijkstra(i)
            self.assertEqual(1, len(distances))
            self.assertEqual(Path(edges=[], cost=0, destination=i), distances[0])

    def test_connected_graph_returns_proper_routes(self):
        g = Graph(5, [
            Edge(origin=0, destination=1, cost=4),
            Edge(origin=1, destination=2, cost=40),
            Edge(origin=1, destination=3, cost=64),
            Edge(origin=0, destination=4, cost=30),
            Edge(origin=4, destination=2, cost=2)
        ])

        cost_expectations = [
            {0: 0, 1: 4, 2: 32, 3: 68, 4: 30},
            {1: 0, 2: 40, 3: 64},
            {2: 0, },
            {3: 0},
            {2: 2, 4: 0},
        ]
        for i in range(5):
            paths = g.dijkstra(i)
            self.assertEqual(cost_expectations[i], dict((path.destination, path.cost) for path in paths))
