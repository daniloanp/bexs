from http import HTTPStatus

from aiohttp.test_utils import unittest_run_loop
from asynctest import Mock

from src.routes_manager import Route, Itinerary, LocationNotFoundException, \
    OriginSameAsDestinationException
from test.test_web_service.base import BaseTestCase


class TestRoutesResources(BaseTestCase):
    @unittest_run_loop
    async def test_it_call_best_route_properly(self):
        get_best_route = Mock(return_value=Itinerary(
            cost=127,
            routes=(
                Route('ABC', 'DEF', 63),
                Route('DEF', 'GHI', 48),
                Route('GHI', 'XYZ', 16)),
        ))
        self.routes_repository.retrieve_manager.return_value = Mock(get_best_route=get_best_route)
        async with self.client.get('/itinerary', params={'origin': 'BRA', 'destination': 'ARG'}) as response:
            data = await response.json()
            self.assertEqual(
                {'cost': 127,
                 'routes': [
                     {'origin': 'ABC', 'destination': 'DEF', 'cost': 63},
                     {'origin': 'DEF', 'destination': 'GHI', 'cost': 48},
                     {'origin': 'GHI', 'destination': 'XYZ', 'cost': 16}
                 ]
                 },
                data

            )
            self.assertEqual(HTTPStatus.OK, response.status)
            get_best_route.assert_called_once_with('BRA', 'ARG')

    @unittest_run_loop
    async def test_it_must_return_400_for_missing_params(self):
        async with self.client.get('/itinerary') as response:
            self.assertEqual(HTTPStatus.BAD_REQUEST, response.status)

    @unittest_run_loop
    async def test_it_must_return_400_for_invalid_location(self):
        def raise_exp(*_):
            raise LocationNotFoundException('BRA')

        get_best_route = Mock(side_effect=raise_exp)
        locations = Mock(return_value=('BRA', 'URU'))
        self.routes_repository.retrieve_manager.return_value = Mock(get_best_route=get_best_route,
                                                                    locations=locations)
        async with self.client.get('/itinerary', params={'origin': 'BRA', 'destination': 'ARG'}) as response:
            self.assertEqual(HTTPStatus.BAD_REQUEST, response.status)
            get_best_route.assert_called_with('BRA', 'ARG')

    @unittest_run_loop
    async def test_no_itinerary_found_must_return_404(self):
        get_best_route = Mock(return_value=None)
        self.routes_repository.retrieve_manager.return_value = Mock(get_best_route=get_best_route)
        async with self.client.get('/itinerary', params={'origin': 'BRA', 'destination': 'ARG'}) as response:
            self.assertEqual(HTTPStatus.NOT_FOUND, response.status)

    @unittest_run_loop
    async def test_it_must_return_400_for_origin_equals_dest(self):
        def raise_exp(*_):
            raise OriginSameAsDestinationException('BRA')

        get_best_route = Mock(side_effect=raise_exp)
        locations = Mock(return_value=('BRA', 'URU'))
        self.routes_repository.retrieve_manager.return_value = Mock(get_best_route=get_best_route,
                                                                    locations=locations)
        async with self.client.get('/itinerary', params={'origin': 'BRA', 'destination': 'BRA'}) as response:
            self.assertEqual(HTTPStatus.BAD_REQUEST, response.status)
            get_best_route.assert_called_with('BRA', 'BRA')
