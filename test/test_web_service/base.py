from aiohttp.test_utils import AioHTTPTestCase
from asynctest import Mock, patch, CoroutineMock, MagicMock

from src.web_service.app import create_application
from src.web_service.routes_repository import RoutesRepository


class BaseTestCase(AioHTTPTestCase):
    @staticmethod
    def app_path() -> str:
        return "src.web_service.app"

    async def get_application(self):
        return create_application(Mock(routes_file_path='banan.txt'))

    def setUp(self) -> None:
        self.routes_repository = Mock(spec=RoutesRepository)
        self.aiofiles = patch(f'{self.app_path()}.aiofiles',
                              Mock(open=MagicMock(
                                  __aenter__=CoroutineMock(
                                      return_value=CoroutineMock
                                      (__aexit__=CoroutineMock)),
                              )))
        self.patch_routes_repository = patch(f'{self.app_path()}.RoutesRepository',
                                             Mock(return_value=self.routes_repository)
                                             )
        self.aiofiles.start()
        self.patch_routes_repository.start()
        super().setUp()

    def tearDown(self) -> None:
        self.aiofiles.stop()
        self.patch_routes_repository.stop()
        super().tearDown()
