import asyncio

from asynctest import Mock, CoroutineMock, call
from asynctest import TestCase

from src.routes_manager import Route
from src.web_service.routes_repository import RoutesRepository


class TestRoutesRepository(TestCase):
    async def setUp(self) -> None:
        self.rows = [
            'BRA,URU,2',
            'BRA,CAN,34',
            'BRA,ARG,35'
        ]

        async def gen():
            for i in self.rows:
                yield i

        self.file_mock = Mock(seek=CoroutineMock(),
                              __aiter__=Mock(side_effect=gen),
                              write=CoroutineMock())
        self.repository = RoutesRepository(self.file_mock)

    async def test_there_is_no_concurrency_between_operations(self):
        self.file_mock.write.side_effect = [
            lambda *args: asyncio.sleep(.01),
            lambda *args: asyncio.sleep(.005),
            lambda *args: asyncio.sleep(.008),
            lambda *args: asyncio.sleep(.0004),
        ]
        self.file_mock.seek.side_effect = lambda _: asyncio.sleep(.2)

        await asyncio.gather(
            self.repository.load_manager(),
            self.repository.save('BRL', 'BRA', 1),
            self.repository.save('BRL', 'BRB', 5),
            self.repository.save('BRL', 'BRU', 6),
            self.repository.load_manager(),
            self.repository.save('BRL', 'BRAY', 7),
        )

        self.assertEqual(
            [
                call.seek(0),
                call.__aiter__(),  # iterate through lines
                call.write('\nBRL,BRA,1'),
                call.write('\nBRL,BRB,5'),
                call.write('\nBRL,BRU,6'),
                call.seek(0),
                call.__aiter__(),  # iterate through lines
                call.write('\nBRL,BRAY,7')],
            self.file_mock.mock_calls)

    async def test_it_load_when_not_loaded(self):
        manager = await self.repository.retrieve_manager()
        self.assertEqual(
            [
                call.seek(0),
                call.__aiter__(),  # iterate through lines
            ],
            self.file_mock.mock_calls
        )
        self.assertEqual([
            Route(origin='BRA', destination='URU', cost=2),
            Route(origin='BRA', destination='CAN', cost=34),
            Route(origin='BRA', destination='ARG', cost=35)
        ], list(manager.routes()))

    async def test_it_doest_not_reload_when_unchanged(self):
        await asyncio.gather(*(self.repository.retrieve_manager() for _ in range(100)))
        self.assertEqual(
            [
                call.seek(0),
                call.__aiter__(),  # iterate through lines
            ],
            self.file_mock.mock_calls
        )
