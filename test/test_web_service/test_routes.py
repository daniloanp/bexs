from http import HTTPStatus

from aiohttp.test_utils import unittest_run_loop

from src.routes_manager import Route, RoutesManager
from test.test_web_service.base import BaseTestCase


class TestRoutesResources(BaseTestCase):
    @unittest_run_loop
    async def test_it_return_properly(self):
        self.routes_repository.retrieve_manager.return_value = RoutesManager([
            Route('BRA', 'ARG', 2),
            Route('USA', 'CAN', 10),
        ])
        async with self.client.get('/routes') as response:
            data = await response.json()
            self.assertEqual([
                {
                    "cost": 2,
                    "source": "BRA",
                    "destination": "ARG"
                },
                {
                    "cost": 10,
                    "source": "USA",
                    "destination": "CAN"
                }], data)
        self.assertEqual(HTTPStatus.OK, response.status)

    @unittest_run_loop
    async def test_create_resource_route(self):
        async with self.client.post('/routes', json={
            "cost": 2666,
            "origin": "BRA",
            "destination": "ARG"
        }, ) as response:
            self.assertEqual(HTTPStatus.CREATED, response.status)
            self.routes_repository.save.assert_awaited_once_with('BRA', 'ARG', 2666)

    @unittest_run_loop
    async def test_try_create_with_missing_field_returns_400(self):
        async with self.client.post('/routes', json={
            "cost": 2666,
            "destination": "ARG"
        }, ) as response:
            self.assertEqual(HTTPStatus.BAD_REQUEST, response.status)
            self.routes_repository.save.assert_not_awaited()

    @unittest_run_loop
    async def test_different_content_type_must_return_415(self):
        async with self.client.post('/routes', data='dsadsa') as response:
            self.assertEqual(HTTPStatus.UNSUPPORTED_MEDIA_TYPE, response.status)
            self.routes_repository.save.assert_not_awaited()
