from http import HTTPStatus

from aiohttp.test_utils import unittest_run_loop

from test.test_web_service.base import BaseTestCase


class TestBaseEndpoint(BaseTestCase):
    @unittest_run_loop
    async def test_it_return_properly(self):
        async with self.client.get('/') as response:
            data = await response.json()
            self.assertEqual({
                'hostname',
                'os',
                'sys',
                'server_current_timestamp'
            }, set(data.keys()))
            self.assertEqual(HTTPStatus.OK, response.status)
