from io import StringIO
from unittest import TestCase
from unittest.mock import patch

from src.routes_manager import routes_manager_from_file_path, Route, RoutesManager, LocationNotFoundException, \
    OriginSameAsDestinationException


class TestRoutesManager(TestCase):
    def setUp(self) -> None:
        routes = (
            Route(origin='BRA', destination='URU', cost=4),
            Route(origin='URU', destination='ARG', cost=43),
            Route(origin='URU', destination='CHL', cost=64),
            Route(origin='BRA', destination='CHL', cost=29),
            Route(origin='CHL', destination='ARG', cost=3),
            Route(origin='USA', destination='CAN', cost=400),
        )
        self.manager = RoutesManager(routes)

    def test_locations_return_every_referred_origin_and_destination(self):
        self.assertEqual({'BRA', 'URU', 'ARG', 'CHL', 'USA', 'CAN'}, set(self.manager.locations()))

    def test_non_existent_itinerary_returns_none(self):
        self.assertIsNone(self.manager.get_best_route('BRA', 'USA'))

    def test_it_raise_same_origin_as_destination_when_is_invalid(self):
        with self.assertRaises(OriginSameAsDestinationException):
            self.manager.get_best_route('BAN', 'BAN')

    def test_it_raise_invalid_location_when_location_is_not_found(self):
        with self.assertRaises(LocationNotFoundException):
            self.manager.get_best_route('BAN', 'BRU')

    def test_it_find_proper_itinerary(self):
        itinerary = self.manager.get_best_route('BRA', 'URU')
        self.assertEqual(4, itinerary.cost)
        self.assertEqual(
            (('BRA', 'URU'),),
            tuple((route.origin, route.destination) for route in itinerary.routes))
        itinerary = self.manager.get_best_route('BRA', 'ARG')
        self.assertEqual(32, itinerary.cost)
        self.assertEqual(
            (('BRA', 'CHL'), ('CHL', 'ARG')),
            tuple((route.origin, route.destination) for route in itinerary.routes))


class TestCreateRoutesManagerFromFile(TestCase):
    def test_when_creating_from_file_itinerary_are_properly_returned(self):
        file = StringIO('ABC,DEF,6\n' + 'DEF,GHI,7\n' + 'GHI,JKL,8')
        file_path = 'any'

        def my_open(path, mode):
            if mode == 'r' and path == file_path:
                return file
            return None

        with patch('src.routes_manager.open', my_open):
            manager = routes_manager_from_file_path(file_path)
            itinerary = manager.get_best_route('ABC', 'DEF')
            self.assertEqual(6, itinerary.cost)
            itinerary = manager.get_best_route('DEF', 'GHI')
            self.assertEqual(7, itinerary.cost)
            itinerary = manager.get_best_route('GHI', 'JKL')
            self.assertEqual(8, itinerary.cost)
