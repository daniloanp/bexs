from unittest import TestCase
from unittest.mock import Mock, patch

from src.cli.manager import CliManager
from src.routes_manager import RoutesManager, Itinerary, Route, LocationNotFoundException, \
    OriginSameAsDestinationException


class TestCliManager(TestCase):
    def test_it_handle_input_properly(self):
        mocked_manager = Mock(spec=RoutesManager)
        cli_manager = CliManager(manager=mocked_manager)
        mocked_manager.get_best_route.side_effect = [
            Itinerary(cost=10, routes=(Route(cost=10, origin='BRL', destination='URU'),)),
            Itinerary(cost=25, routes=(Route(cost=25, origin='BAB', destination='BAB'),)),
            None
        ]

        my_input_mock = Mock(side_effect=['BRL-URU', 'BAB-DED', 'BEB-DUD', KeyboardInterrupt])

        with patch("src.cli.manager.input", my_input_mock), patch('src.cli.manager.print') as my_print:
            my_print.side_effect = print
            cli_manager.input_loop()
        self.assertEqual(3, mocked_manager.get_best_route.call_count)
        self.assertEqual(('BRL', 'URU'), mocked_manager.get_best_route.mock_calls[0].args)
        self.assertEqual(('BAB', 'DED'), mocked_manager.get_best_route.mock_calls[1].args)
        self.assertEqual(('BEB', 'DUD'), mocked_manager.get_best_route.mock_calls[2].args)

        self.assertEqual(4, my_print.call_count)
        self.assertEqual('best route: BRL - URU > $10', ''.join(my_print.mock_calls[0].args))
        self.assertEqual('best route: BAB - BAB > $25', ''.join(my_print.mock_calls[1].args))
        self.assertEqual('no route found', ''.join(my_print.mock_calls[2].args))
        self.assertEqual('\n\n>>>thank you for using our system, good bye!!!',
                         ''.join(my_print.mock_calls[3].args))

    def test_it_prints_wrong_location_properly(self):
        mocked_manager = Mock(spec=RoutesManager)
        cli_manager = CliManager(manager=mocked_manager)

        def raise_location_not_found(source, _):
            raise LocationNotFoundException(source)

        mocked_manager.get_best_route.side_effect = raise_location_not_found
        mocked_manager.locations.return_value = ('BRA', 'URU')

        my_input_mock = Mock(side_effect=['BRL-URU', KeyboardInterrupt])

        with patch("src.cli.manager.input", my_input_mock), \
                patch('src.cli.manager.print') as my_print:
            my_print.side_effect = print
            cli_manager.input_loop()
        self.assertEqual(1, mocked_manager.get_best_route.call_count)
        self.assertEqual(('BRL', 'URU'), mocked_manager.get_best_route.mock_calls[0].args)

        self.assertEqual(2, my_print.call_count)
        self.assertEqual("invalid location BRL, please enter one of the following: BRA, URU",
                         ' '.join(my_print.mock_calls[0].args))
        self.assertEqual('\n\n>>>thank you for using our system, good bye!!!',
                         ''.join(my_print.mock_calls[1].args))

    def test_it_handle_origin_same_as_destination_properly(self):
        mocked_manager = Mock(spec=RoutesManager)
        cli_manager = CliManager(manager=mocked_manager)

        def raise_location_not_found(source, _):
            raise OriginSameAsDestinationException(source)

        mocked_manager.get_best_route.side_effect = raise_location_not_found
        mocked_manager.locations.return_value = ('BRA', 'URU')

        my_input_mock = Mock(side_effect=['BRL-URU', KeyboardInterrupt])

        with patch("src.cli.manager.input", my_input_mock), \
                patch('src.cli.manager.print') as my_print:
            my_print.side_effect = print
            cli_manager.input_loop()
        self.assertEqual(1, mocked_manager.get_best_route.call_count)
        self.assertEqual(('BRL', 'URU'), mocked_manager.get_best_route.mock_calls[0].args)

        self.assertEqual(2, my_print.call_count)
        self.assertEqual("origin and destination are the same:  BRL please try again!",
                         ' '.join(my_print.mock_calls[0].args))
        self.assertEqual('\n\n>>>thank you for using our system, good bye!!!',
                         ''.join(my_print.mock_calls[1].args))

    def test_it_handle_invalid_input_properly(self):
        mocked_manager = Mock(spec=RoutesManager)
        cli_manager = CliManager(manager=mocked_manager)

        mocked_manager.locations.return_value = ('BRA', 'URU')

        my_input_mock = Mock(side_effect=['BRL=URU', KeyboardInterrupt])

        with patch("src.cli.manager.input", my_input_mock), \
                patch('src.cli.manager.print') as my_print:
            my_print.side_effect = print
            cli_manager.input_loop()
        self.assertEqual(0, mocked_manager.get_best_route.call_count)

        self.assertEqual(2, my_print.call_count)
        self.assertEqual("invalid input, please enter according to ORG-DST",
                         ' '.join(my_print.mock_calls[0].args))
        self.assertEqual('\n\n>>>thank you for using our system, good bye!!!',
                         ''.join(my_print.mock_calls[1].args))
